/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Cliente;
import model.Pan;
import utiliy.Keyboard;

import java.util.Arrays;

/**
 * @author yolima
 */
public class Panaderia {

    public static Keyboard keyboard = new Keyboard();

    private Cliente[] clientes;
    private Pan[] despensa;
    private int clienteActual;
    private int[] ventas;

    public Panaderia() {
        clientes = new Cliente[0];
        ventas = new int[100];
        initDespensa();
        initVentas();
    }

    public Cliente[] getClientes() {
        return clientes;
    }

    public Cliente getClienteActual() {
        return clientes[clienteActual - 1];
    }

    public int[] getVentas() {
        return ventas;
    }

    public void setVentas(int[] ventas) {
        this.ventas = ventas;
    }

    public void agregarCliente(Cliente client) throws ArrayIndexOutOfBoundsException {
        incrementarClientes();
        client.setIdCliente(clienteActual-1);
        clientes[clienteActual-1] = client;
    }

    // Este método pide la cantidad de cada tipo de pan
    public void venderPan() {
        imprimirPanes();

        for (Pan pan : despensa) {
            int numeroPanes = keyboard.readValidPositiveInteger("\n  Ingrese la cantidad de " + pan.getTipo() + ": ");
            for (int j = 0; j < numeroPanes; j++) {
                clientes[clienteActual - 1].agregarPan(pan);
            }
        }
        ventas[clienteActual-1] = clientes[clienteActual - 1].calcularTotal();
        System.out.println("\n  Su total es $" + ventas[clienteActual-1]);
    }

    public void imprimirPanes() {
        System.out.println("\n Tipos de pan a la venta: ");
        for (Pan pan : despensa) System.out.println("   " + pan.getTipo() + ": $" + pan.getPrecio());
    }

    public void initDespensa() {
        despensa = new Pan[4];
        Pan pan = new Pan("Mogolla", 200);
        Pan pan1 = new Pan("Integral", 300);
        Pan pan2 = new Pan("Francés", 500);
        Pan pan3 = new Pan("Pan coco", 400);

        despensa[0] = pan;
        despensa[1] = pan1;
        despensa[2] = pan2;
        despensa[3] = pan3;
    }

    public void initVentas(){
        Arrays.fill(ventas, 0);
    }

    public void printVentas(){
        for (int venta: ventas) {
            System.out.println(venta);
        }
    }

    public int obtenerPrecioCliente(){
        return clientes[clienteActual-1].calcularTotal();
    }

    public int calcularTotalVentas(){
        int totalVentas = 0;
        try{
            for (int venta: ventas) {
                totalVentas += venta;
            }
        }catch(NullPointerException ignored){ }

        return totalVentas;
    }

    private void incrementarClientes() {
        Cliente[] newArray = new Cliente[++clienteActual];
        if (clienteActual > 0) {
            System.arraycopy(clientes, 0, newArray, 0, clienteActual - 1);
            clientes = newArray;
        }
    }

    public Cliente getClientByCardNumber(int cedula) {
        for (Cliente client : clientes) {
            if (client.getCedula() == cedula) {
                return client;
            }
        }
        return null;
    }

    public boolean isClientRegistered(int cedula) {
        for (Cliente client : clientes) {
            if (client.getCedula() == cedula) {
                return true;
            }
        }
        return false;
    }

}
