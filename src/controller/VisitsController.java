package controller;

import model.Cliente;
import model.Visita;

public class VisitsController {

    public final int VISITS_SIZE = 100;
    public final int DAYS_NUMBER = 7;

    private final Visita[][] visitsClients;
    private int positionSize;

    public VisitsController() {
        visitsClients = new Visita[VISITS_SIZE][DAYS_NUMBER];
        positionSize = 0;

        initVisitsClients();
    }

    public void setPositionSize(int positionSize) {
        this.positionSize = positionSize;
    }

    private void initVisitsClients() {
        for (int i = 0; i < VISITS_SIZE; i++) {
            for (int j = 0; j < DAYS_NUMBER; j++) {
                visitsClients[i][j] = new Visita();
            }
        }
    }

    public void registerVisitClient(Cliente cliente, int currentDay, int hora, int precio) {
        Visita visita = new Visita();

            visita.setCliente(cliente);
            visita.setHora(hora);
            visita.setPrecio(precio);

            visitsClients[positionSize][currentDay] = visita;
            positionSize++;
    }

    public String getDay(int currentDay) {
        String weekday;
        switch (currentDay) {
            case 0 -> weekday = "Lunes";
            case 1 -> weekday = "Martes";
            case 2 -> weekday = "Miércoles";
            case 3 -> weekday = "Jueves";
            case 4 -> weekday = "Viernes";
            case 5 -> weekday = "Sábado";
            case 6 -> weekday = "Domingo";
            default -> {
                return null;
            }
        }
        return weekday;
    }

    public void printVisits() {
        for (int i = 0; i < DAYS_NUMBER; i++) {
            try {
                System.out.print("\nVisitas del " + getDay(i) + ": \n");
                for (int j = 0; j < VISITS_SIZE; j++) {
                    System.out.print(visitsClients[j][i].toString() + " \n");
                }
            } catch (NullPointerException ignored) {
            }
        }
    }
}
