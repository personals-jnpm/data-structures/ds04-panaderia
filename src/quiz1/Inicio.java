package quiz1;

import controller.Panaderia;
import controller.VisitsController;
import model.Cliente;
import utiliy.Keyboard;

public class Inicio {

    private static final Panaderia tienda = new Panaderia();
    public static VisitsController visitsController = new VisitsController();

    public static Keyboard keyboard = new Keyboard();
    public static boolean estadoTienda = true;

    public static int currentDay = 0;

    public static void main(String[] args) {

        // añadir panes
        // vender pan: registrar cada cliente que va llegando y decirle el total

        //corregir errores (sintaxis y de lógica)

        //agregar try/catch correcta y realizar la guía de error para que no se dañe el programa

        menu();
        int option;
        do {
            System.out.print("\nSeleccione una opción en el menú principal: ");
            option = keyboard.readIntegerDefault(-1);
            switch (option) {
                case 0 -> System.out.println(" El programa ha finalizado");
                case 1 -> addClient();
                case 2 -> finishDay();
                case 3 -> printVisits();
                case 4 -> showVisitsByDay();
                default -> System.out.println(" ¡Opción no disponible en el menú principal!");
            }
        } while (option != 0 && estadoTienda);
        System.out.println("\nEl total vendido durante la semana es: " + tienda.calcularTotalVentas());
    }

    public static void menu() {
        System.out.println("╔═════════════════════════════════════════════════════╗");
        System.out.println("╠----------------------Panadería----------------------╣");
        System.out.println("║═════════════════════════════════════════════════════║");
        System.out.println("║   1. Atender cliente                                ║");
        System.out.println("║   2. Finalizar día                                  ║");
        System.out.println("║   3. Mostrar las visitas semanales                  ║");
        System.out.println("║   4. Mostrar visitas de un cliente en un día        ║");
        System.out.println("║   0. Salir                                          ║");
        System.out.println("╚═════════════════════════════════════════════════════╝");
    }

    public static void addClient() {
        int cedula = keyboard.readValidPositiveInteger("  Ingrese la cédula del cliente: ");
        Cliente cliente = new Cliente();

        if (tienda.isClientRegistered(cedula)) {
            cliente = tienda.getClientByCardNumber(cedula);
        } else {
            cliente.setCedula(cedula);
            System.out.print("  Ingrese el nombre del cliente: ");
            String nombre = keyboard.readLine();
            cliente.setNombre(nombre);

        }
        registrarCliente(cliente);
    }

    public static void registrarCliente(Cliente cliente) {
        try {
            tienda.agregarCliente(cliente);
            tienda.venderPan();
            agregarVisita(cliente);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("  ¡Lista de clientes llena!");
            System.out.println("\nLa tienda ha cerrado");
            estadoTienda = false;
        }
    }

    public static void agregarVisita(Cliente cliente) {
        int horaActual = keyboard.readValidHour();
        visitsController.registerVisitClient(cliente, currentDay, horaActual, tienda.obtenerPrecioCliente());
    }

    public static void finishDay() {
        if (currentDay < 6) {
            currentDay += 1;
            System.out.println(" Día actual " + visitsController.getDay(currentDay));
            visitsController.setPositionSize(0);
        } else {
            printVisits();
            estadoTienda = false;
        }
    }

    public static void showVisitsByDay() {
        System.out.println("In process");
    }

    public static void printVisits() {
        visitsController.printVisits();
    }

}
