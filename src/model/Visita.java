package model;

public class Visita {

    private Cliente cliente;
    private int precio;
    private int hora;
    private int dia;

    public Visita() {
    }

    public Visita(Cliente cliente, int precio, int hora, int dia) {
        this.cliente = cliente;
        this.precio = precio;
        this.hora = hora;
        this.dia = dia;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    @Override
    public String toString() {
        return "Visita { " +
                "\n  cliente = " + cliente.getNombre() +
                "\n  precio = $" + precio +
                "\n  hora = " + hora +
                "\n}";
    }
}
