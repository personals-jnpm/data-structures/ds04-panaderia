package model;

/**
 *
 * @author yolima
 */
public class Cliente {

    private int idCliente;
    private int cedula;
    private String nombre;
    public Pan[] canasta;

    public Cliente() {
        canasta = new Pan[0];
    }

    public Cliente(int idCliente, int cedula, String nombre, Pan[] canasta) {
        this.idCliente = idCliente;
        this.cedula = cedula;
        this.nombre = nombre;
        this.canasta = canasta;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Pan[] getCanasta() {
        return canasta;
    }

    public void setCanasta(Pan[] Canasta) {
        this.canasta = Canasta;
    }

    public void agregarPan(Pan pan){
        try{
            incrementarCanasta();
            getCanasta()[canasta.length-1] = pan;
        }catch(ArrayIndexOutOfBoundsException e){
            System.out.println("  Canasta del cliente llena, máximo 50 panes");
        }
    }

    public int calcularTotal(){
        int total = 0;
        try{
            for (Pan pan: canasta) {
                total += pan.getPrecio();
            }
        }catch(NullPointerException ignored){ }
        return total;
    }

    private void incrementarCanasta() {
        int limite = canasta.length;
        Pan[] nuevaCanasta = new Pan[++limite];
        if (limite > 0) {
            System.arraycopy(canasta, 0, nuevaCanasta, 0, limite - 1);
            canasta = nuevaCanasta;
        }
    }

}
