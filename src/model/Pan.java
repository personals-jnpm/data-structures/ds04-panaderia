package model;

/**
 *
 * @author yolima
 */
public class Pan {
    
    private String tipo;
    private int precio;

    public Pan() {
    }

    public Pan(String tipo, int precio) {
        this.tipo = tipo;
        this.precio = precio;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Pan{" +
                "tipo='" + tipo + '\'' +
                ", precio=" + precio +
                '}';
    }
}
